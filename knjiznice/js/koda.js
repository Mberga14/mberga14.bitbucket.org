
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija



function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val(); // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = $("#kreirajPriimek").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezo").val();
	var ehrId;

	if (!ime || !priimek || !visina || !teza || ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2017-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "36.50";
							var sistolicniKrvniTlak = "118";
							var diastolicniKrvniTlak = "92";
							var nasicenostKrviSKisikom = "98";
							var merilec = "Martin B.";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { 
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}



function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function izracunajBMI() {
	sessionId= getSessionId();
	
	var ehrId = $("#preberiEHRid").val();
	
	$.ajax({
		url: baseUrl + "/view/" + ehrId +"/" + "weight",
		type : 'GET',
		headers: {
        "Ehr-Session": sessionId
    },
    success: function(res) {
    	if(res.length > 0){
    		for (var i in res){
    			var teza = res[i].weight;
    		}	
    	}
    	
    $.ajax({
    	url: baseUrl + "/view/" + ehrId + "/" + "height",
    	type: 'GET',
    	headers: {
        "Ehr-Session": sessionId
    },
    success: function(res) {
    	if(res.length>0){
    		for(var i in res){
    			var visina = res[i].height / 100;
    		}
    	}
    	var BMI = teza/ (visina*visina);
    	document.getElementById('vpisiBMI').value= new Number(BMI.toFixed(2));
    	
    	if (BMI > 0 && BMI < 20){
    		$("#vpisiPovpBMI").html("<span class='obvestilo label label-danger fade-in'>Vaš BMI je podpovrečen!");
    	} else if (BMI > 20 && BMI < 30){
    		$("#vpisiPovpBMI").html("<span class='obvestilo label label-danger fade-in'>Vaš BMI je povrečen!");
    	}else if (BMI > 30){
    		$("#vpisiPovpBMI").html("<span class='obvestilo label label-danger fade-in'>Vaš BMI je previsok! Svetujemo obisk zdravnika.");
    	}
    	
    	
    	
    }
    
    })
   
    }
    
    
	});

}


          var map;

            function initMap() {
              if (navigator.geolocation) {
                try {
                  navigator.geolocation.getCurrentPosition(function(position) {
                    var myLocation = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    };
                    setPos(myLocation);
                  });
                } catch (err) {
                  var myLocation = {
                    lat: 46.8701334,
                    lng: 15.2713944
                  };
                  setPos(myLocation);
                }
              } else {
                var myLocation = {
                  lat: 46.8701334,
                  lng: 15.2713944
                };
                setPos(myLocation);
              }
            }

            function setPos(myLocation) {
              map = new google.maps.Map(document.getElementById('map'), {
                center: myLocation,
                zoom: 10
              });
              
              var mojMarker= new google.maps.Marker({
              	map: map,
              	position: myLocation
              })

              var service = new google.maps.places.PlacesService(map);
              service.nearbySearch({
                location: myLocation,
                radius: 4000,
                types: ['hospital']
              }, processResults);

            }

            function processResults(results, status, pagination) {
              if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
              } else {
                createMarkers(results);

              }
            }

            function createMarkers(places) {
              var bounds = new google.maps.LatLngBounds();
              var placesList = document.getElementById('places');

              for (var i = 0, place; place = places[i]; i++) {
                var image = {
                  url: place.icon,
                  size: new google.maps.Size(71, 71),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(17, 34),
                  scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                  map: map,
                  icon: image,
                  title: place.name,
                  animation: google.maps.Animation.DROP,
                  position: place.geometry.location
                });

                bounds.extend(place.geometry.location);
              }
              map.fitBounds(bounds);
            }




